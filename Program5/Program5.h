/*
* Author: Caleb Nasman
* COMP 322 Program 5
* This file defines the header for the main window
*/

#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Program5.h"
#include "BMP_Handler.h"

class Program5 : public QMainWindow
{
	Q_OBJECT

public:
	Program5(QWidget *parent = Q_NULLPTR);

public slots:
	void loadFile(void);
	void saveFile(void);
	void decodeImage(void);
	void encodeImage(void);
	void textEditChanged(void);

signals:
	void sendPixmap(QPixmap);
	void sendDecodedMsg(QString);
	void imageLoaded(bool);
	void setCountLbl(QString);
private:
	Ui::Program5Class ui;
	char unsigned *imageData;
	int width, height; // dimensions image
	int size; // number of bytes to store image
};
