/*
* Author: Caleb Nasman
* COMP 322 Program 5
* This file is the header for the Steganographic encoder/decoder
*/

#pragma once

class SteganographyCoder {
public:
	// encodes the msg into the least significant bits of the data
	static void encode(unsigned char* data, const char* msg, int dataSize, int msgSize);
	// returns a pointer to a c string containing the message stored in the data and sets msgSize to 
	// the respective length of the char.  If there is no message, the function returns null and the msgSize is set to -1
	static char* decode(const unsigned char* data, int dataSize, int &msgSize);
	// max length of bytes that can be stored inside the data
	static int maxMessageSize(int dataSize);
private:
	// chosen to be a value that doesn't typically naturally appear
	static const char flag = 20;
	// reads a byte formed by the least significant bits of 8 consecutive bytes of data, starting at the pointer
	static char readByte(const unsigned char* data);
	// encodes the byte into the least signicant bits of 8 consecutive bytes of data, starting at the pointer
	static void writeByte(unsigned char* data, const char byte);
};