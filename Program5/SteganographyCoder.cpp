/*
* Author: Caleb Nasman
* COMP 322 Program 5
* This file implements StaganographyCoder
*/

#include "SteganographyCoder.h"
#include <vector>
using std::vector;

int SteganographyCoder::maxMessageSize(int size) {
	// need to use to first byte as a flag and null char at end
	int msgSize = size / 8 - 2;
	// message size can't be less than zero
	msgSize = (msgSize < 0) ? 0 : msgSize;
	return msgSize;
}

void SteganographyCoder::encode(unsigned char* data, const char* msg, int dataSize, int msgSize) {
	// need at least 2 bytes of least sig. bits to store a message (1 byte char with a 1-byte flag)
	if (dataSize < 16) {
		return;
	}
	
	// write flag to indicate we are storing a message
	writeByte(data, flag);
	// write the message one char at a time
	int i;
	for (i = 0; (i < msgSize+1) && (i + 1) * 8 < dataSize; i++) {
		writeByte(data + 8 * (i+1), msg[i]);
	}
	// if we ran out of room, replace the last character with '\0'
	if (i < msgSize +1) {
		i--;
		writeByte(data + 8 * (i+1), '\0');
	}
}

char* SteganographyCoder::decode(const unsigned char* data, int dataSize, int &msgSize) {
	msgSize = -1;
	// need at least 2 bytes of least sig. bits to store a message (1 byte char with a 1-byte flag)
	if (dataSize < 16) {
		return NULL;
	} // check if data contains a message 
	else {
		char c = readByte(data);
		if (c != flag) 
			return NULL;
	}

	vector<char> bytes;
	int i = 1;
	char byte = readByte(data + 8);
	// keep reading until we hit null or end of the data
	while (byte != '\0' && (i+1)*8 <= dataSize) {
		bytes.push_back(byte);
		i++;
		byte = readByte(data + 8 * i);
	}
	bytes.push_back('\0');
	
	// copy vector to dynamic array
	msgSize = bytes.size();
	char* msg = new char[msgSize];
	for (int i = 0; i < msgSize; ++i) {
		msg[i] = bytes[i];
	}

	return msg;
}

char SteganographyCoder::readByte(const unsigned char* data) {
	char byte = 0;
	// grab the last bits of first 8 bytes
	for (int i = 0; i < 8; i++) {
		byte |= (data[i] & 0x1) << i;
	}
	return byte;
}

void SteganographyCoder::writeByte(unsigned char* data, const char val) {
	// set the least significant bits (LSBs) of the first 8 bytes, starting at data
	for (int i = 0; i < 8; i++) {
		data[i] &= 0xFE; // set last bit to zero
		data[i] |= ((val >> i) & 0x1); // set the LSB of the i'th byte to the i'th bit in val
	}
}