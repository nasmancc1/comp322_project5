/*
* Author: Caleb Nasman
* COMP 322 Program 5
* This file implements the functionality of the main window
*/

#include "Program5.h"
#include "SteganographyCoder.h"
#include <QFileDialog>
#include <QString>
#include <string>

using std::string;
using std::to_string;

Program5::Program5(QWidget *parent)
	: QMainWindow(parent)
{
	imageData = NULL;
	width = -1;
	height = -1;
	size = -1;
	ui.setupUi(this);
}

void Program5::loadFile(void) {
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QString(),tr("Images (*.bmp)"));
	
	if (!fileName.isNull()) {
		string fn = fileName.toStdString();
		if (imageData != NULL)
			delete[] imageData;
		imageData = BMP_Handler::loadBMP(fn.c_str(), width, height);
		size = width * height * 3; // each pixel is 3 bytes

		textEditChanged();
		emit sendPixmap(QPixmap(fileName));
		emit imageLoaded(true);
	}
}

void Program5::saveFile(void) {
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), QString(), tr("Images (*.bmp)"));
	if (!fileName.isNull()) {
		string fn = fileName.toStdString();
		BMP_Handler::saveBMP(fn.c_str(), imageData, width, height);
	}
}

void Program5::decodeImage(void) {

	int msgSize = -1;
	char* msg = SteganographyCoder::decode(imageData, size, msgSize);
	QString msgStr = QString(msg);
	if (msg != NULL) {
		delete[] msg;
	}
	emit sendDecodedMsg(msgStr);
}

void Program5::encodeImage(void) {
	QString msgStr = ui.textEdit->toPlainText();
	string msg = msgStr.toStdString();
	SteganographyCoder::encode(imageData, msg.c_str(), size, msg.length());
}

void Program5::textEditChanged(void) {
	if (imageData != NULL) { // ensure a message has been loaded
		int maxMsgSize = SteganographyCoder::maxMessageSize(size);
		QString inputText = ui.textEdit->toPlainText();

		string countLbl = "(" + to_string(maxMsgSize - inputText.length())
			+ " characters left out of " + to_string(maxMsgSize) +")";

		emit setCountLbl(QString(countLbl.c_str()));
	}
}